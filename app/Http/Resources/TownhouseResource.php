<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class TownhouseResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'bedroom_count' => $this->bedroom_count,
            'bathroom_count' => $this->bathroom_count,
            'storey_count' => $this->storey_count,
            'garage_count' => $this->garage_count,
        ];
    }
}
