<?php

namespace App\Http\Requests;

use App\Dto\TownhouseSearchDto;

class TownhousesRequest extends Request
{
    public function rules(): array
    {
        return [
            'name' => 'nullable|string',
            'bedroom_count' => 'nullable|integer|min:0',
            'bathroom_count' => 'nullable|integer|min:0',
            'storey_count' => 'nullable|integer|min:0',
            'garage_count' => 'nullable|integer|min:0',
            'min_price' => 'nullable|integer|min:0',
            'max_price' => 'nullable|integer|min:0',
        ];
    }

    public function data(): TownhouseSearchDto
    {
        $dto = new TownhouseSearchDto();
        $dto->name = $this->query('name');
        $dto->bedroom_count = $this->query('bedroom_count');
        $dto->bathroom_count = $this->query('bathroom_count');
        $dto->storey_count = $this->query('storey_count');
        $dto->garage_count = $this->query('garage_count');
        $dto->min_price = $this->query('min_price');
        $dto->max_price = $this->query('max_price');
        return $dto;
    }
}
