<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\TownhousesRequest;
use App\Http\Resources\TownhouseResource;
use App\Services\TownhouseService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
* @OA\Info(
*      version="1.0.0",
*      title="Townhouse Api",
* )
*
*/

/**
 * @OA\Tag(name="Townhouse")
 */
class TownhouseController extends Controller
{
    private TownhouseService $townhouseService;

    public function __construct(TownhouseService $townhouseService){
        $this->townhouseService = $townhouseService;
    }

    /**
     * @OA\Get(
     *     path="/api/townhouses",
     *     description="Get list of townhouses",
     *     tags={"Townhouse"},
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="string"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="bedroom_count",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="number"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="bathroom_count",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="number"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="storey_count",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="number"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="garage_count",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="number"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="min_price",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="number"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="max_price",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="number"),
     *         style="form"
     *     ),
     *  @OA\Response(
     *         response="200",
     *         description="Success",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                      "data": {{
     *                         "id": 1,
     *                         "name": "Towhnhouse name...",
     *                         "price": 100000,
     *                         "bedroom_count": 1,
     *                         "bathroom_count": 1,
     *                         "storey_count": 1,
     *                         "garage_count": 1,
     *                      }}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(response=400, description="Bad request")
     * )
     */
    public function index(TownhousesRequest $request): AnonymousResourceCollection
    {
        $result = $this->townhouseService->searchForTownhouses($request->data());
        return TownhouseResource::collection($result);
    }
}
