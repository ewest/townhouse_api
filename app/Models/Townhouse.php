<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $bedroom_count
 * @property int $bathroom_count
 * @property int $storey_count
 * @property int $garage_count
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Townhouse  extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ['name', 'price', 'bedroom_count', 'bathroom_count', 'storey_count', 'garage_count'];
}
