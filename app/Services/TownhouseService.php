<?php

namespace App\Services;


use App\Dto\TownhouseSearchDto;
use App\Models\Townhouse;
use Illuminate\Database\Eloquent\Collection;

class TownhouseService
{
    public function searchForTownhouses(TownhouseSearchDto $dto): Collection
    {
        $query = Townhouse::query()->select();
        if(isset($dto->name)) {
            $query->where('name', 'ilike', '%' . $dto->name . '%');
        }
        if(isset($dto->bathroom_count)) {
            $query->where('bathroom_count', '=', $dto->bathroom_count);
        }
        if(isset($dto->bedroom_count)) {
            $query->where('bedroom_count', '=', $dto->bedroom_count);
        }
        if(isset($dto->garage_count)) {
            $query->where('garage_count', '=', $dto->garage_count);
        }
        if(isset($dto->storey_count)) {
            $query->where('storey_count', '=', $dto->storey_count);
        }
        if(isset($dto->min_price)) {
            $query->where('price', '>=', $dto->min_price);
        }
        if(isset($dto->max_price)) {
            $query->where('price', '<=', $dto->max_price);
        }
        return $query->get();
    }
}
