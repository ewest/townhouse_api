<?php

namespace App\Dto;

class TownhouseSearchDto {
    public ?string $name;
    public ?int $bedroom_count;
    public ?int $bathroom_count;
    public ?int $storey_count;
    public ?int $garage_count;
    public ?int $min_price;
    public ?int $max_price;
}
