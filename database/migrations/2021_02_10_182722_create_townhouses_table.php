<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTownhousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('townhouses', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('name');
            $table->bigInteger('price');
            $table->integer('bedroom_count');
            $table->integer('bathroom_count');
            $table->integer('storey_count');
            $table->integer('garage_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('townhouses');
    }
}
