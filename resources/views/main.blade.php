<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Townhouse') }}</title>
        <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
        <script src="{{ asset(mix('js/app.js')) }}"></script>
    </head>
    <body>
        <div id="app">
            <townhouse-search></townhouse-search>
        </div>
    </body>
</html>
