require('./bootstrap');

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import elementUi from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';

Vue.use(elementUi, { locale });
Vue.use(VueAxios, axios);
Vue.component('townhouse-search', require('./components/townhouse-search/townhouse-search.vue').default);
window.addEventListener('load', function () {
    new Vue().$mount('#app');
});

